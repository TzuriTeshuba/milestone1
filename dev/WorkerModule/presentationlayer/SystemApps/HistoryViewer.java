package WorkerModule.presentationlayer.SystemApps;

import WorkerModule.LogicLayer.ServiceManager;
import WorkerModule.presentationlayer.dataObjects.BranchData;
import WorkerModule.presentationlayer.dataObjects.ShiftData;

import java.time.LocalDate;
import java.util.List;

public class HistoryViewer extends SystemApp {
    public HistoryViewer() {
        System.out.println("Hist viewer");
    }

    @Override
    public void start() {
        Callback[] menu = getMainMenu();
        while(!shouldTerminate){
            Callback choice = displayMenu(menu);
            while(choice == null)choice = displayMenu(menu);
            choice.call();
        }
    }

    protected Callback[] getMainMenu() {
        Callback[] menu = {
                new Callback("view employee history") {
                    @Override
                    public boolean call() {
                        return viewEmployeeHistory();
                    }
                },
                new Callback("view branch history") {
                    @Override
                    public boolean call() {
                        return viewBranchHistory();
                    }
                },
                new Callback("view franchise history") {
                    @Override
                    public boolean call() {
                        return viewFranchiseHistory();
                    }
                },
                new Callback("exit") {
                    @Override
                    public boolean call() {
                        return exit();
                    }

                }
        };
        return menu;
    }

    private boolean exit() {
        shouldTerminate = true;
        return false;
    }

    private boolean viewFranchiseHistory() {
        LocalDate[] range = getDateRange();
        LocalDate from = range[0];
        LocalDate to = range[1];

        //convert to data objects and print
        List<ShiftData> shiftDataList = service.getFranchiseHistory(from, to);
        ShiftData prev=null;
        if(!shiftDataList.isEmpty())prev=shiftDataList.get(0);
        else System.out.println("no history to show");
        for (ShiftData shift : shiftDataList){
            if (shift.getBranchId() != prev.getBranchId()) System.out.println("-----------------------------------");
            prev = shift;
            System.out.println(shift);
        }
        return true;
    }

    public boolean viewBranchHistory() {
        int branchId = getBranchId();
        LocalDate[] range = getDateRange();
        LocalDate from = range[0];
        LocalDate to = range[1];
        List<ShiftData> shiftDataList = service.getBranchHistory(branchId, from, to);
        for (ShiftData s : shiftDataList) System.out.println(s);
        return true;
    }

    public boolean viewEmployeeHistory() {
        System.out.println("Enter employee of interest");
        String str = scanner.nextLine();
        int id = tryParsePositive(str, null);
        while (id < 0) {
            System.out.println("id must be positive decimal number, try again");
            id = tryParsePositive(scanner.nextLine(), null);
        }
        while(service.getEmployee(id)==null){
            System.out.println("Employee not recognized by system, try again");
            id = tryParsePositive(scanner.nextLine(), null);
        }
        //get range
        LocalDate[] range = getDateRange();
        LocalDate from = range[0];
        LocalDate to = range[1];

        //convert to data objects and print
        List<ShiftData> shiftDataList = service.getEmployeeHistory(id, from, to);
        if(shiftDataList.isEmpty()) System.out.println("no history to show");
        for (ShiftData s : shiftDataList) System.out.println(s);
        return true;
    }


}
