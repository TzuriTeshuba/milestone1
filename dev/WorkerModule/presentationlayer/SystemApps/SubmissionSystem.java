package WorkerModule.presentationlayer.SystemApps;

import WorkerModule.Constants;
import WorkerModule.LogicLayer.ServiceManager;

import java.time.LocalDate;

public class SubmissionSystem extends SystemApp {
    private static final int SUBMISSION_WINDOW = Constants.SUBMISSION_WINDOW;
    private static final int NUM_SHIFT_IN_DAY = Constants.NUM_SHIFT_IN_DAY;

    public SubmissionSystem() {

    }
    @Override
    public void start() {
        Callback[] menu = getMainMenu();
        while(!shouldTerminate){
            Callback choice = displayMenu(menu);
            while(choice == null)choice = displayMenu(menu);
            choice.call();
        }
    }

    @Override
    protected Callback[] getMainMenu() {
        Callback[] menu = {
                new Callback("submit shifts") {
                    @Override
                    public boolean call() {
                        return submitShifts();
                    }
                },
                new Callback("view submitted shifts") {
                    @Override
                    public boolean call() {
                        return viewConstraints();
                    }},
                new Callback("exit") {
                    @Override
                    public boolean call() {
                        return exit();
                    }}
        };
        return menu;
    }

    private boolean exit() {
        shouldTerminate = true;
        return false;
    }

    private boolean submitShifts(){
        boolean[][] availability = null;
        while (availability == null) {
            System.out.println("please enter shifts that you can work.");
            displayDates();
            String s = scanner.nextLine();
            availability = parseConstraints(s);
            if (availability == null) System.out.println("invalid format, try again, ex: 2M,2E,3e,5m");
        }
        return ServiceManager.getInstance().submitShifts(user.getId(), user.getBranchId(), availability);

    }

    //parses string like 0m,2e,3m,3e  can be big or small letters
    //e for evening, m for morning, no spaces
    private boolean[][] parseConstraints(String s) {
        boolean[][] output = new boolean[SUBMISSION_WINDOW][NUM_SHIFT_IN_DAY];
        String[] sArr = s.split(",");
        for (String str : sArr) {
            int time = -1;
            char c = str.charAt(str.length() - 1);
            if (c == 'm' | c == 'M') time = 0;
            else if (c == 'e' | c == 'E') time = 1;
            else return null;
            String dateIdxStr = str.substring(0, str.length() - 1);
            int dateIdx = tryParsePositive(dateIdxStr, "")-1;
            if (dateIdx < 0 | dateIdx >= SUBMISSION_WINDOW) return null;
            output[dateIdx][time] = true;
        }
        return output;
    }

    private boolean viewConstraints() {
        LocalDate date;
        boolean[][] constraints = ServiceManager.getInstance().getConstraints(user.getId(), user.getBranchId());
        System.out.println("your current availability is:");
        System.out.println("\t\t\t  Morn  Eve");
        for (int i = 0; i < constraints.length; i++) {
            date = LocalDate.now().plusDays(i);
            System.out.print("" + date.getDayOfWeek().toString().substring(0, 3) + "\t" + date + "\t");
            for (int time = 0; time < NUM_SHIFT_IN_DAY; time++) {
                if (constraints[i][time] == true) System.out.print("V\t");
                else System.out.print(" \t");
                if (time == NUM_SHIFT_IN_DAY - 1) System.out.println("");
            }
        }
        return true;
    }

    private void displayDates() {
        LocalDate date = LocalDate.now().plusDays(1);
        for (int i = 0; i < SUBMISSION_WINDOW; i++) {
            date = date.plusDays(i);
            System.out.println("" + (i+1) + ") " + date.getDayOfWeek().toString().substring(0,3) + " " + date);
        }
    }


}
