package WorkerModule.presentationlayer.SystemApps;

import WorkerModule.Constants;
import WorkerModule.Enums.Position;
import WorkerModule.Enums.ShiftTime;
import WorkerModule.LogicLayer.Employee;
import WorkerModule.presentationlayer.dataObjects.EmployeeData;
import WorkerModule.presentationlayer.dataObjects.ShiftData;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import static WorkerModule.Constants.NUM_SHIFT_IN_DAY;

public class ShiftAssemblerSystem extends SystemApp {
    private ShiftData currShift;

    public ShiftAssemblerSystem() {
        System.out.println("shift assembler");
    }

    public static final int SCHEDULE_WINDOW = Constants.SCHEDULE_WINDOW;

    @Override
    protected Callback[] getMainMenu() {
        Callback[] menu = {
                new Callback("view schedule") {
                    @Override
                    public boolean call() {
                        return viewSchedule();
                    }
                },
                new Callback("get availability for range") {
                    @Override
                    public boolean call() {
                        return showAvailabilityForRange();
                    }
                },
                new Callback("exit") {
                    @Override
                    public boolean call() {
                        return exit();
                    }
                }
        };
        return menu;
    }

    private boolean exit() {
        shouldTerminate = true;
        return false;
    }

    private boolean showAvailabilityForRange() {
        LocalDate[] range = getDateRange();
        int branchId = getBranchId();
        List<List<EmployeeData>> emps2D = service.getAvailability(range[0], range[1], branchId);
        System.out.println("----------------------------------------");
        int day = 0;
        int time = 0;
        for (List<EmployeeData> emps : emps2D) {
            LocalDate date = LocalDate.now().plusDays(day);
            System.out.println(date.getDayOfWeek().toString().substring(0, 3) + " " + date +
                    " " + ShiftTime.values()[time] + ":");
            emps.sort(Comparator.comparing(e -> e.getPositions().get(0)));
            for (EmployeeData emp : emps) {
                System.out.println(emp.getName() + " " + emp.getPositions());
            }
            System.out.println("----------------------------------------");
            time++;
            if (time == NUM_SHIFT_IN_DAY) time = 0;
            if (time == 0) day++;
        }
        return false;
    }


    private boolean viewSchedule() {
        int branchId = getBranchId();
        ShiftData[][] schedule = service.getSchedule(branchId);
        System.out.println("choose day to display: ");
        for (int i = 0; i < SCHEDULE_WINDOW; i++) {
            System.out.println((i + 1) + ") " + LocalDate.now().plusDays(i));
        }
        int dayIndex = -1;
        while (dayIndex == -1) dayIndex = checkValidIndex(scanner.nextLine(), SCHEDULE_WINDOW);
        System.out.println("choose morning shift or evening shift:");
        for (ShiftTime shiftTime : ShiftTime.values())
            System.out.println((shiftTime.ordinal() + 1) + ") " + shiftTime.toString());
        int shiftIndex = -1;
        while (shiftIndex == -1) shiftIndex = checkValidIndex(scanner.nextLine(), NUM_SHIFT_IN_DAY);
        this.currShift = schedule[dayIndex][shiftIndex];
        return showShift();
    }

    private boolean showShift() {
        if (currShift == null) System.out.println("shift is not been scheduled yet");
        else if(hasManager() || forceManager()) System.out.println(currShift);
        Callback[] menu = {
                new Callback("view another shift") {
                    @Override
                    public boolean call() {
                        return viewSchedule();
                    }
                },
                new Callback("get shift availability") {
                    @Override
                    public boolean call() {
                        return getAvailabilityForShift();
                    }
                },
                new Callback("edit shift") {
                    @Override
                    public boolean call() {
                        return editShift();
                    }
                },
                new Callback("exit") {
                    @Override
                    public boolean call() {
                        return exitShiftView();
                    }
                }
        };

        return displayMenu(menu).call();
    }
    private boolean forceManager(){//returns false if manager was/could not be scheduled
        List<EmployeeData> availableManagers= service.getAvailableManagers(currShift);
        if(availableManagers.isEmpty()){
            System.out.println("no managers are available to work shift, try again later");
            return false;
        }
        else System.out.println("No manager is scheduled for this shift yet, please schedule at least 1:");
        //printAvailability(availableManagers);
        int limit = availableManagers.size();
        int i = 1;
        for (EmployeeData man : availableManagers) {
            System.out.println("" + i + ") " + man.getName() + "\t" + man.getPositions());
            i++;
        }
        int[] toAddIdx = null;
        while(toAddIdx == null) toAddIdx = parseConstraints(scanner.nextLine(), limit);
        long[] manIds = new long[toAddIdx.length];
        for (i = 0; i < manIds.length; i++) {
            manIds[i] = availableManagers.get(i).getId();
        }
        if(manIds.length == 0) forceManager();
        else service.addEmployeesToShift(currShift,manIds);
        currShift = service.getShiftFromSchedule(currShift);
        return true;
    }

    private boolean exitShiftView() {
        return false;
    }

    private boolean editShift() {
        return showShiftEditMenu();
    }

    private boolean getAvailabilityForShift() {
        List<EmployeeData> emps = service.getAvailabilty(currShift);
        printAvailability(emps);
        return showShiftEditMenu();
    }

    private void printAvailability(List<EmployeeData> emps) {
        System.out.println(currShift.getDate().getDayOfWeek().toString().substring(0, 3) + " " + currShift.getDate() +
                " " + currShift.getShifttime() + ":");
        emps.sort(Comparator.comparing(e -> e.getPositions().get(0)));
        int i=1;
        for (EmployeeData emp : emps) {
            System.out.println(""+i+") "+emp.getName() + "\t" + emp.getPositions());
            i++;
        }
        System.out.println("");
    }

    private boolean showShiftEditMenu() {
        Callback[] menu = {
                new Callback("get shift availability") {
                    @Override
                    public boolean call() {
                        return getAvailabilityForShift();
                    }
                },
                new Callback("add employees to shift") {
                    @Override
                    public boolean call() {
                        return addEmployeesToShift();
                    }
                },
                new Callback("remove employees from shift") {
                    @Override
                    public boolean call() {
                        return removeEmployeesFromShift();
                    }
                },
                new Callback("exit") {
                    @Override
                    public boolean call() {
                        return showShift();
                    }
                }
        };
        return displayMenu(menu).call();
    }


    private boolean removeEmployeesFromShift() {
        System.out.println("select employees to remove:");
        long[] empIds = getEmpIdsFromShift();
        service.removeEmployeesFromShift(currShift, empIds);
        currShift = service.getShiftFromSchedule(currShift);
        if(!hasManager())forceManager();
        System.out.println(currShift);
        return showShiftEditMenu();
    }

    private boolean addEmployeesToShift() {
        System.out.println("select employees to add:");
        long[] empIds = getAvailableEmpsToAdd();
        service.addEmployeesToShift(currShift, empIds);
        currShift = service.getShiftFromSchedule(currShift);
        System.out.println(currShift);
        return showShiftEditMenu();
    }

    private long[] getAvailableEmpsToAdd() {
        List<EmployeeData> emps = service.getAvailabilty(currShift);
        emps.sort(Comparator.comparing(e -> e.getPositions().get(0)));
        int limit = emps.size();
        int i = 1;
        for (EmployeeData emp : emps) {
            System.out.println("" + i + ") " + emp.getName() + "\t" + emp.getPositions());
            i++;
        }
        int[] toAddIdx = null;
        while(toAddIdx == null) toAddIdx = parseConstraints(scanner.nextLine(), limit);
        long[] empIds = new long[toAddIdx.length];
        for (i = 0; i < empIds.length; i++) {
            empIds[i] = emps.get(i).getId();
        }
        return empIds;
    }


    private long[] getEmpIdsFromShift() {
        int limit = currShift.getStaff().size();
        int i = 1;
        for (EmployeeData emp : currShift.getStaff()) {
            System.out.println("" + i + ") " + emp.getName() + "\t" + emp.getPositions());
            i++;
        }
        int[] toRemoveIdx = null;
        while (toRemoveIdx==null)toRemoveIdx = parseConstraints(scanner.nextLine(),limit);
        long[] empIds = new long[toRemoveIdx.length];
        for (i = 0; i < empIds.length; i++) {
            empIds[i] = currShift.getStaff().get(i).getId();
        }
        return empIds;
    }

    private int[] parseConstraints(String s, int limit) {
        String[] sArr = s.split(",");
        int[] output = new int[sArr.length];
        for (int i = 0; i < sArr.length; i++) {
            output[i] = tryParsePositive(sArr[i], "must provide positive decimal value");
            if (output[i] < 1) {
                System.out.println("all entries must be positive numbers seperated by comma (no spaces)");
                return null;
            }
            if (output[i] > limit) {
                System.out.println("entries can not be greater than " + limit);
                return null;
            }
        }
        return output;
    }


    @Override
    public void start() {
        Callback[] menu = getMainMenu();
        while (!shouldTerminate) {
            Callback c = displayMenu(menu);
            while (c == null) c = displayMenu(menu);
            while (c.call()) {
            }
        }
    }
    private boolean hasManager(){
        for(EmployeeData emp : currShift.getStaff()){
            if(emp.getPositions().contains(Position.SHIFT_MANAGER))return true;
        }
        return false;
    }

}
