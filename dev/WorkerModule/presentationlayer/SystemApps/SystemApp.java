package WorkerModule.presentationlayer.SystemApps;

import WorkerModule.LogicLayer.ServiceManager;
import WorkerModule.presentationlayer.dataObjects.BranchData;
import WorkerModule.presentationlayer.dataObjects.EmployeeData;

import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;

public abstract class SystemApp {
    protected Scanner scanner;
    protected boolean shouldTerminate;
    protected EmployeeData user;
    protected ServiceManager service;

    public SystemApp() {
        this.scanner = new Scanner(System.in);
        this.shouldTerminate = false;
        service = ServiceManager.getInstance();
        service.updateHistory();
    }

    protected abstract Callback[] getMainMenu();

    public Callback displayMenu(Callback[] menu) {
    /*
    return boolen instead of void
    and call the function again if return false
     */
        for (int i = 0; i < menu.length; i++) {
            System.out.println("" + (i + 1) + ") " + menu[i].menuName);
        }
        String str = scanner.nextLine();
        int choice = -1;
        try {
            choice = Integer.parseInt(str);
        } catch (Exception e) {
            System.out.println("not a decimal number, try again");
            return null;
        }
        if ((choice <= 0) | (choice > (menu.length))) {
            System.out.println("must be number between 1 and " + (menu.length));
            return null;
        }
        return menu[choice - 1];
    }


    public void setUser(EmployeeData user) {
        this.user = user;
    }

    public abstract void start();

    public static int tryParsePositive(String str, String errorMsg) {
        int num = -1;
        try {
            num = Integer.parseInt(str);
        } catch (Exception e) {
            if (errorMsg != null) {
                System.out.println(errorMsg);
                return -1;
            }
        }
        if(num < 1 ){
            System.out.println(errorMsg);
            return -1;
        }
        return num;
    }

    protected LocalDate tryParseDate(String str) {
        LocalDate output = null;
        try {
            output = LocalDate.parse(str);
        } catch (Exception e) {
            return null;
        }
        return output;
    }

    public static int checkValidIndex(String index, int listSize) {
        int result = tryParsePositive(index, "index must be positive decimal number");
        if (result < 1 | result > listSize) {
            if (result != -1) System.out.println("index is not in range try again");
            return -1;
        }
        return result - 1;
    }

    protected boolean askYesOrNoQuestion(String question) {
        System.out.println(question + " [y,n]");
        boolean yesORno = false;
        while (true) {
            String answer = scanner.nextLine();
            if (answer.equals("y")) return true;
            else if (answer.equals("n")) return false;
            else System.out.println("must enter y,n answer!");
        }

    }

    protected LocalDate[] getDateRange() {
        LocalDate from, to;
        System.out.println("Provide start date in YYYY-MM-DD format");
        from = tryParseDate(scanner.nextLine());
        while (from == null) {
            System.out.println("improper format");
            from = tryParseDate(scanner.nextLine());
        }

        System.out.println("Provide end date in YYYY-MM-DD format");
        to = tryParseDate(scanner.nextLine());
        while (to == null) {
            System.out.println("improper format");
            to = tryParseDate(scanner.nextLine());
        }
        return new LocalDate[]{from, to};
    }
     protected int getBranchId(){
         List<BranchData> branches = service.getBranches();
         System.out.println("choose branch: ");
         for(int i = 0; i < branches.size(); i++) System.out.println((i+1) + ")" + branches.get(i).getName());
         int branchNum = -1;
         while(branchNum == -1)branchNum = checkValidIndex(scanner.nextLine(), branches.size());
         int branchId = branches.get(branchNum).getId();
         return branchId;
     }

}
