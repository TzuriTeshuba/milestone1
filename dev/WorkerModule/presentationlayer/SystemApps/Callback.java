package WorkerModule.presentationlayer.SystemApps;

public abstract class Callback {
    public String menuName;

    public Callback(String s) {
        this.menuName = s;
    }

    public abstract boolean call();
}
