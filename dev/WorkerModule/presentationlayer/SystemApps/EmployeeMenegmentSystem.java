package WorkerModule.presentationlayer.SystemApps;

import WorkerModule.Enums.Position;
import WorkerModule.presentationlayer.dataObjects.BranchData;
import WorkerModule.presentationlayer.dataObjects.EmployeeData;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class EmployeeMenegmentSystem extends SystemApp {
    public EmployeeMenegmentSystem() {
        scanner = new Scanner(System.in);
    }


    @Override
    public void start() {
        Callback[] menu = getMainMenu();
        while (!shouldTerminate) {
            Callback choise = displayMenu(menu);
            while (choise == null) choise = displayMenu(menu);
            choise.call();
        }
    }

    @Override
    protected Callback[] getMainMenu() {
        Callback[] menu = {
                new Callback("view existing employees") {
                    @Override
                    public boolean call() {
                        return viewExistingEmployeeMenu();
                    }
                },
                new Callback("add new employee") {
                    @Override
                    public boolean call() {
                        return viewAddEmployeeMenu();
                    }
                },
                new Callback("delete an employee") {
                    @Override
                    public boolean call() {
                        return deleteEmployeeMenu();
                    }
                },
                new Callback("edit an existing employee") {
                    @Override
                    public boolean call() {
                        return editExistingEmployeeMenu();
                    }
                },
                new Callback("exit") {
                    @Override
                    public boolean call() {
                        shouldTerminate = true;
                        return true;
                    }
                }
        };
        return menu;
    }

    private boolean editExistingEmployeeMenu() {
        Callback[] menu = {
                new Callback("all employees") {
                    @Override
                    public boolean call() {
                        return editAllEmployees();
                    }
                },
                new Callback("employees by branch") {
                    @Override
                    public boolean call() {
                        return editByBranches();
                    }
                },
                new Callback("exit") {
                    @Override
                    public boolean call() {
                        return false;
                    }
                }
        };
        Callback choise = displayMenu(menu);
        while (choise == null) {
            choise = displayMenu(menu);
        }
        return choise.call();
    }

    private boolean editByBranches() {
        return chooseEmployeeToEdit(chooseBranchReturnEmployees());
    }

    private boolean editAllEmployees() {
        List<EmployeeData> allEmpyees = service.getAllDataEmployees();
        return chooseEmployeeToEdit(allEmpyees);
    }

    private boolean chooseEmployeeToEdit(List<EmployeeData> empoyees) {
        System.out.println("choose employee's number to edit to:");
        EmployeeData emp = chooseEmplyeeFromList(empoyees);

        if (editEmployee(emp)) return true;
        else {
            boolean ans = askYesOrNoQuestion("wish to edit another employee?");
            if (ans) return editExistingEmployeeMenu();
            else return true;
        }
    }

    private EmployeeData chooseEmplyeeFromList(List<EmployeeData> empoyees) {
        for (int i = 0; i < empoyees.size(); i++) {
            EmployeeData emp = empoyees.get(i);
            System.out.println((i + 1) + ") " + emp.getName() + " - " + emp.getId());
        }
        int empNumber = -1;
        while (empNumber == -1) {
            String s = scanner.nextLine();
            empNumber = checkValidIndex(s, empoyees.size());
        }
        return empoyees.get(empNumber);
    }

    private boolean editEmployee(EmployeeData emp) {
        System.out.println("you chose : " + emp.getName());
        boolean ans = askYesOrNoQuestion("did you wish to edit this employee?");
        if (!ans) return false;
        System.out.println("employee info:\n" + emp.toString());
        System.out.println("what would you wish to change?");
        Callback[] menu = createEditMenu(emp);
        Callback choise = displayMenu(menu);
        while (choise == null) {
            choise = displayMenu(menu);
        }
        choise.call();
        return true;
    }

    private Callback[] createEditMenu(EmployeeData emp) {
        Callback[] menu = {
                new Callback("change name") {
                    @Override
                    public boolean call() {
                        return changeEmpName(emp);
                    }
                },
                new Callback("change id") {
                    @Override
                    public boolean call() {
                        return changeEmpId(emp);
                    }
                },
                new Callback("change bank account number") {
                    @Override
                    public boolean call() {
                        return changeEmpBankAccount(emp);
                    }
                },
                new Callback("change salary") {
                    @Override
                    public boolean call() {
                        return changeSalary(emp);
                    }
                },
                new Callback("change conditions") {
                    @Override
                    public boolean call() {
                        return changeEmpconditions(emp);
                    }
                },
                new Callback("change positions") {
                    @Override
                    public boolean call() {
                        return changeEmpPositions(emp);
                    }
                },
                new Callback("change branch id") {
                    @Override
                    public boolean call() {
                        return changeBranchId(emp);
                    }
                },
                new Callback("exit") {
                    @Override
                    public boolean call() {
                        return false;
                    }
                }
        };
        return menu;
    }

    private boolean changeBranchId(EmployeeData emp) {
        System.out.println("enter new branch number:");
        int newBranch = -1;
        boolean success = false;
        while (!success) {
            String s = scanner.nextLine();
            newBranch = tryParsePositive(s, "not a legal branch id try again");
            if (newBranch != -1) {
                if (service.changeEmpBranch(emp.getId(), emp.getBranchId(), newBranch))
                    emp.setBranchId(newBranch);
                return true;
            }
        }
        return false;
    }

    private boolean changeEmpPositions(EmployeeData emp) {
        Callback[] menu = {
                new Callback("set employ as manger") {
                    @Override
                    public boolean call() {
                        return service.setEmployeeAsManager(emp.getId(), emp.getBranchId());
                    }
                },
                new Callback("change position") {
                    @Override
                    public boolean call() {
                        return changePosigion(emp);
                    }
                },
                new Callback("exit") {
                    @Override
                    public boolean call() {
                        return false;
                    }
                }
        };
        Callback choise = displayMenu(menu);
        while (choise == null) {
            choise = displayMenu(menu);
        }
        choise.call();
        return true;
    }

    private boolean changePosigion(EmployeeData emp) {
        System.out.println("choose new position: ");
        for (Position pos : Position.values())if(pos != Position.SHIFT_MANAGER) System.out.println(pos.ordinal() + ") " + pos);
        int index = -1;
        while (index == -1) index = checkValidIndex(scanner.nextLine(), Position.values().length - 1);
        service.changeEmployeePosition(emp.getId(), emp.getBranchId(), Position.values()[index + 1]);
        return false;
    }


    private boolean changeEmpconditions(EmployeeData emp) {
        System.out.println("enter new conditions:");
        String newCondiotions = scanner.nextLine();
        emp.setConditions(newCondiotions);
        service.saveEmpoyee(emp);
        return true;
    }

    private boolean changeSalary(EmployeeData emp) {
        System.out.println("enter new salary:");
        int newSalary = -1;
        while (newSalary == -1) {
            String s = scanner.nextLine();
            newSalary = tryParsePositive(s, "not a legal salary try again");
        }
        emp.setSalary(newSalary);
        service.saveEmpoyee(emp);
        return true;
    }

    private boolean changeEmpBankAccount(EmployeeData emp) {
        System.out.println("enter new bank account:");
        int newAccount = -1;
        while (newAccount == -1) {
            String s = scanner.nextLine();
            newAccount = tryParsePositive(s, "not a legal bank account try again");
        }
        emp.setBankAccountNumber(newAccount);
        service.saveEmpoyee(emp);
        return true;
    }

    private boolean changeEmpId(EmployeeData emp) {
        System.out.println("enter new id:");
        int newId = -1;
        while (newId == -1) {
            String s = scanner.nextLine();
            newId = tryParsePositive(s, "not a legal id");
        }
        if (service.changeEmpoyeeId(emp.getId(), emp.getBranchId(), newId)) return true;
        System.out.println("id already in the system try again");
        return changeEmpId(emp);
    }

    private boolean changeEmpName(EmployeeData emp) {
        System.out.println("enter new name:");
        String newName = scanner.nextLine();
        emp.setName(newName);
        service.saveEmpoyee(emp);
        return true;
    }

    private boolean deleteEmployeeMenu() {
        Callback[] menu = {
                new Callback("all employees") {
                    @Override
                    public boolean call() {
                        List<EmployeeData> employees = service.getAllDataEmployees();
                        return deleteUserFromList(employees);
                    }
                },
                new Callback("employees by branch") {
                    @Override
                    public boolean call() {
                        List<EmployeeData> employees = chooseBranchReturnEmployees();
                        return deleteUserFromList(employees);
                    }
                },
                new Callback("exit") {
                    @Override
                    public boolean call() {
                        return false;
                    }
                }
        };
        System.out.println("choose to delete from: ");
        Callback choise = displayMenu(menu);
        while (choise == null) {
            choise = displayMenu(menu);
        }
        return choise.call();
    }

    private boolean viewAddEmployeeMenu() {
        System.out.println("please insert new employee info:");
        System.out.print("name: ");
        String name = scanner.nextLine();
        System.out.print("id: ");
        int id = -1;
        while (id == -1) {
            int idToCheck = tryParsePositive(scanner.nextLine(), "not a valid id");
            if (service.checkEmployeeIdAvailability(idToCheck)) id = idToCheck;
            else System.out.println("id is already in use!");
        }
        System.out.print("salary: ");
        int salary = -1;
        while (salary == -1) salary = tryParsePositive(scanner.nextLine(), "not a valid salary");
        List<BranchData> branches = service.getBranches();
        System.out.println("choose branch number: ");
        for (int i = 0; i < branches.size(); i++) System.out.println((i + 1) + ")" + branches.get(i).getName());
        int branchNum = -1;
        while (branchNum == -1) branchNum = checkValidIndex(scanner.nextLine(), branches.size());
        int branchid = branches.get(branchNum).getId();
        System.out.print("bank account number: ");
        int bankAccountNumber = -1;
        while (bankAccountNumber == -1)
            bankAccountNumber = tryParsePositive(scanner.nextLine(), "not a valid bank account");
        System.out.print("conditions: ");
        String conditions = scanner.nextLine();
        LocalDate firstDate = LocalDate.now();
        List<Position> positions = new LinkedList<>();
        System.out.println("choose position: ");
        for (Position pos : Position.values())if(pos != Position.SHIFT_MANAGER) System.out.println(pos.ordinal() + ") " + pos);
        int posNum = -1;
        while (posNum == -1) posNum = checkValidIndex(scanner.nextLine(), Position.values().length - 1) ;
        positions.add(Position.values()[posNum + 1]);
        boolean isManger = askYesOrNoQuestion("set as shift manger?");
        if (isManger) positions.add(Position.SHIFT_MANAGER);
        EmployeeData newEmployee = new EmployeeData(name, id, bankAccountNumber, salary, conditions, positions, firstDate, branchid);
        return service.createNewEmployee(newEmployee);
    }


    private boolean deleteUserFromList(List<EmployeeData> employees) {
        System.out.println("choose employee's number to delete:");
        EmployeeData empToDel = chooseEmplyeeFromList(employees);
        boolean ans = askYesOrNoQuestion("are you sure you want to delete this employee?");
        if(!ans)return true;
        return service.deleteEmployee(empToDel.getId(), empToDel.getBranchId());
    }

    private List<EmployeeData> chooseBranchReturnEmployees() {
        List<BranchData> branches = service.getBranches();
        System.out.println("please choose a branch:");
        for (int i = 0; i < branches.size(); i++) System.out.println((i + 1) + ") " + branches.get(i).getName());
        List<EmployeeData> emplyees = null;
        while (emplyees == null) {
            int branchID = checkValidIndex(scanner.nextLine(), branches.size());
            if (branchID != -1) emplyees = branches.get(branchID).getEmployees();
        }
        return emplyees;
    }

    private boolean viewExistingEmployeeMenu() {
        Callback[] menu = {
                new Callback("all employees") {
                    @Override
                    public boolean call() {
                        for (EmployeeData emp : service.getAllDataEmployees()) System.out.println(emp.toString());
                        return true;
                    }
                },
                new Callback("employees by branch") {
                    @Override
                    public boolean call() {
                        for (EmployeeData emp : chooseBranchReturnEmployees()) System.out.println(emp.toString());
                        return true;
                    }
                },
                new Callback("exit") {
                    @Override
                    public boolean call() {
                        return false;
                    }
                }

        };
        Callback choise = displayMenu(menu);
        while (choise == null) {
            choise = displayMenu(menu);
        }
        return choise.call();
    }
}