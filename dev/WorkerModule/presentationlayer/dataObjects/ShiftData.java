package WorkerModule.presentationlayer.dataObjects;

import WorkerModule.Enums.ShiftTime;

import java.time.LocalDate;
import java.util.List;

public class ShiftData {

    //fields
    private List<EmployeeData> staff;
    private LocalDate date;
    private ShiftTime shiftTime;
    private int branchId;


    //getters and setters
    public List<EmployeeData> getStaff() {
        return staff;
    }

    public void setStaff(List<EmployeeData> staff) {
        this.staff = staff;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public ShiftTime getShifttime() {
        return shiftTime;
    }

    public void setShifttime(ShiftTime shifttime) {
        this.shiftTime = shifttime;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public ShiftData(List<EmployeeData> staff, ShiftTime shifttime, LocalDate date, int branchId) {
        this.staff = staff;
        this.shiftTime = shifttime;
        this.date = date;
        this.branchId = branchId;
    }

    public String toString(){
        String output =  "Branch: "+branchId+"\t"+date.toString() +"\t"+ this.shiftTime.toString()+" staff:\n";
        for(EmployeeData emp : staff){
            output += emp.getName()+" :\t"+emp.getPositions()+"\t"+emp.getId()+"\n";
        }
        return output;
    }


}
