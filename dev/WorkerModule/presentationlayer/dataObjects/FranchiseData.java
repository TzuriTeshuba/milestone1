package WorkerModule.presentationlayer.dataObjects;

import java.util.List;

public class FranchiseData {

    private List<BranchData> Branches;
    private String name;

    public List<BranchData> getBranches() {
        return Branches;
    }

    public void setBranches(List<BranchData> branches) {
        Branches = branches;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public FranchiseData(List<BranchData> branches, String name) {
        Branches = branches;
        this.name = name;
    }

}
