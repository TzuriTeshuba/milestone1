package WorkerModule.presentationlayer.dataObjects;

import WorkerModule.Enums.Position;

import java.time.LocalDate;
import java.util.List;

public class EmployeeData {


    //fields:
    private String name;
    private long id;
    private int BankAccountNumber;
    private int salary;
    private LocalDate firstDate;
    private String conditions;
    private List<Position> positions;
    private int branchId;


    //getters and setters:
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getBankAccountNumber() {
        return BankAccountNumber;
    }

    public void setBankAccountNumber(int bankAccountNumber) {
        BankAccountNumber = bankAccountNumber;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    public List<Position> getPositions() {
        return positions;
    }

    public void setPositions(List<Position> positions) {
        this.positions = positions;
    }

    public LocalDate getFirstDate() {
        return firstDate;
    }

    public void setFirstDate(LocalDate firstDate) {
        this.firstDate = firstDate;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }



    //constructors
    public EmployeeData(String name,
                        long id,
                        int bankAccountNumber,
                        int salary,
                        String conditions,
                        List<Position> positions,
                        LocalDate firstDate,
                        int branchId)
    {
        this.name = name;
        this.id = id;
        BankAccountNumber = bankAccountNumber;
        this.salary = salary;
        this.conditions = conditions;
        this.positions = positions;
        this.firstDate = firstDate;
        this.branchId = branchId;
    }
    public String toString(){
        String posit = "";
        for(Position pos : positions)posit += pos.toString() + " ";
        return "name: "+ name + "\n" +
                "id: " + id + "\n" +
                "bank account number: " + BankAccountNumber + "\n" +
                "salary: " + salary+ "\n" +
                "first date: " + firstDate.toString() + "\n" +
                "conditions: " + getConditions() + "\n" +
                "positions: " + posit + "\n" +
                "branch id: " + branchId + "\n";
    }
}
