package WorkerModule.presentationlayer.dataObjects;

import java.util.List;

public class BranchData {

    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<EmployeeData> getEmployees() {
        return employees;
    }

    public void setEmployees(List<EmployeeData> employees) {
        this.employees = employees;
    }

    private List<EmployeeData> employees;

    public BranchData(int id, String name, List<EmployeeData> employees) {
        this.id = id;
        this.name = name;
        this.employees = employees;
    }


}
