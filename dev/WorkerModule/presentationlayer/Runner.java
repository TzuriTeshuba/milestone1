package WorkerModule.presentationlayer;

import WorkerModule.Constants;
import WorkerModule.Enums.*;
import WorkerModule.LogicLayer.Employee;
import WorkerModule.LogicLayer.ServiceManager;
import WorkerModule.presentationlayer.SystemApps.SystemApp;
import WorkerModule.presentationlayer.dataObjects.EmployeeData;

import java.util.*;

import static WorkerModule.presentationlayer.SystemApps.SystemApp.checkValidIndex;
import static WorkerModule.presentationlayer.SystemApps.SystemApp.tryParsePositive;

public class Runner {


    //fields:
    private static final HashMap<Class<? extends SystemApp>, String> SystemNameMap = Constants.SystemNameMap;
    private ServiceManager service;
    private boolean shouldTerminate;
    private SystemApp currentApp;
    private EmployeeData currentUser;
    private boolean loggedIn;
    private Map<Position, List<Class<? extends SystemApp>>> access;
    private Scanner scanner;
    private int mode;

    public Runner(int mode) {
        this.mode = mode;
        this.scanner = new Scanner(System.in);
        shouldTerminate = false;
        loggedIn = false;
        this.access = Constants.accesses;
    }

    public void run() {
        service = ServiceManager.getInstance();
        service.initData(mode);
        while (!shouldTerminate) {
            currentUser = login();
            while (loggedIn) {
                currentApp = displayMenu();
                if (currentApp != null) {
                    currentApp.setUser(currentUser);
                    currentApp.start();
                }
            }
        }
    }


    private EmployeeData login() {
        EmployeeData emp = null;
        System.out.print("Enter your id (or q to quit program): ");
        while (emp == null) {
            int id = -1;
            while (id == -1){
                String command = scanner.nextLine();
                if(command.equals("q")){
                    loggedIn =false;
                    shouldTerminate = true;
                    return null;
                }
                id = tryParsePositive(command, "not a valid id, please try again");
            }
            emp = service.getEmployee(id);
            if (emp == null) System.out.println("Id not recognized in system, try again.");
        }
        loggedIn = true;
        System.out.println("welcome " + emp.getName());
        return emp;
    }

    public void logout() {
        loggedIn = false;
    }

    private SystemApp displayMenu() {
        System.out.println("Please choose a system:");
        int idx = 1;
        List<Class<? extends SystemApp>> menu = new ArrayList<>();
        for (Position pos : currentUser.getPositions()) {
            for(Class c : access.get(pos))
                if(!menu.contains(c))
                    menu.add(c);
        }
        for (int i = 0; i < menu.size(); i++) {
            System.out.println("" + (i + 1) + ") " + SystemNameMap.get(menu.get(i)));
        }
        int choice = -1;
        while (choice == -1) choice = checkValidIndex(scanner.nextLine(), menu.size());
        Class<? extends SystemApp> sysAppClass = menu.get(choice);
        if (sysAppClass == null) {
            logout();
            return null;
        }
        SystemApp sysApp = null;
        try {
            sysApp = sysAppClass.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return sysApp;
    }
}
