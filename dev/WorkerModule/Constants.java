package WorkerModule;

import WorkerModule.Enums.Position;
import WorkerModule.presentationlayer.SystemApps.*;

import java.util.*;

public class Constants {
    public static final HashMap<Class<? extends SystemApp>,String> SystemNameMap
            = new HashMap<Class<? extends SystemApp>, String>() {{
        put(SubmissionSystem.class, "Submit schedule constraints");
        put(EmployeeMenegmentSystem.class, "Manage employees");
        put(ShiftAssemblerSystem.class, "Set work schedule");
        put(HistoryViewer.class, "View shift history");
        put(null, "Logout");
    }};

    private static final List<Class<? extends SystemApp>> manAccess = Arrays.asList(EmployeeMenegmentSystem.class,HistoryViewer .class, ShiftAssemblerSystem .class, SubmissionSystem.class, null);
    private static final List<Class<? extends SystemApp>> regAccess = Arrays.asList(SubmissionSystem.class, null);
    public static final Map<Position, List<Class<? extends SystemApp>>> accesses
            = new HashMap<Position, List<Class<? extends SystemApp>>>() {{
        put(Position.HR_MANAGER, manAccess);
        put(Position.CASHIER, regAccess);
        put(Position.DRIVER, regAccess);
        put(Position.GUARD, regAccess);
        put(Position.STOCK, regAccess);
        put(Position.SHIFT_MANAGER,regAccess);
    }};



    public static final Calendar CALENDAR = Calendar.getInstance();
    public static final int MONTHS_HISTORY_BACKUP = 6;
    public static final int SUBMISSION_WINDOW = 14;
    public static final int NUM_SHIFT_IN_DAY = 2;
    public static final int SCHEDULE_WINDOW = 14;

    //just for submission purposes
    public static final int AUTO_INIT = 1;
    public static final int INIT_EMPTY = 2;


}
