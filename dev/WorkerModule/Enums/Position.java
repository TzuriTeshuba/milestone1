package WorkerModule.Enums;

public enum Position {
    SHIFT_MANAGER{
        @Override
        public String toString(){return "Shift Manager";}
    },
    CASHIER{
        @Override
        public String toString(){return "Cashier";}
    },
    DRIVER{
        @Override
        public String toString(){return "Driver";}
    },
    STOCK{
        @Override
        public String toString(){return "Stock";}
    },
    GUARD{
        @Override
        public String toString(){return "Guard";}
    },
    HR_MANAGER{
        @Override
        public String toString(){return "HR Manager";}
    }
}
