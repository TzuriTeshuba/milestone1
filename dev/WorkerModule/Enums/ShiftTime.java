package WorkerModule.Enums;

public enum ShiftTime {
    MORNING{
        @Override
        public String toString(){return "Morning";}
    },
    EVENING{
        @Override
        public String toString(){return "Evening";}
    }
}
