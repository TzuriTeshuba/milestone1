package WorkerModule;

import WorkerModule.presentationlayer.Runner;

import java.util.Scanner;

public class Main {

    public static void main(String []args){
        System.out.println("Welcome to the Worker Module 1.0!");
        boolean shouldAutoInit = false;
        int choice = 0;
        while(choice == 0){
            System.out.println("please choose a mode to run:\n" +
                    "1) Auto Initialize\n" +
                    "2) Fresh Start");
            Scanner scanner = new Scanner(System.in);
            String answer = scanner.nextLine();
            if(answer.equals("1"))choice=Constants.AUTO_INIT;
            else if(answer.equals("2"))choice=Constants.INIT_EMPTY;
        }

        Runner runner = new Runner(choice);
        runner.run();
        System.out.println("safe exit");
    }
}
