package WorkerModule.LogicLayer;

import WorkerModule.Enums.ShiftTime;
import WorkerModule.Enums.Position;
//import WorkerModule.presentationlayer.dataObjects.EmployeeData;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Franchise {
    private String name;
    private Map<Integer, Branch> branches;

    public Franchise() {

    }



    public Franchise(String name, Map<Integer, Branch> branches) {
        this.name = name;
        this.branches = branches;
    }

    public Branch getEmployeeBranch(int empId) {
        Branch output = null;
        for (Branch branch : branches.values()) {
            if (branch.getEmployee(empId) != null) {
                output = branch;
                break;
            }
        }
        return output;
    }

    public Employee getEmployee(int empId, int branchId) {
        return branches.get(branchId).getEmployee(empId);
    }


    public Map<LocalDate, boolean[]> getConstraints(long empId, int branchId) {
        return branches.get(branchId).getConstraints(empId);
    }

    public boolean submitShifts(long empId, int branchId, boolean[][] newConstraints) {
        return branches.get(branchId).updateConstraints(empId, newConstraints);
    }


    //Begin Region Getters and SETTERS

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<Integer, Branch> getBranches() {
        return branches;
    }

    public Branch getBranch(int branchId) {
        return branches.get(branchId);
    }

    public void setBranches(Map<Integer, Branch> branches) {
        this.branches = branches;
    }

    public List<Employee> getAllStaff() {
        List<Employee> staff = new LinkedList<>();
        for (Branch branch : branches.values()) staff.addAll(branch.getStaff());
        return staff;
    }

    public void addEmployeesToShift(LocalDate date, ShiftTime shifttime, int branchId, long[] empIds) {
        getBranch(branchId).addEmployeesToShift(date, shifttime, empIds);
    }

    public void removeEmployeesFromShift(LocalDate date, ShiftTime shifttime, int branchId, long[] empIds) {
        getBranch(branchId).removeEmployeesFromShift(date, shifttime, empIds);
    }

    public List<Employee> getAvailability(LocalDate date, ShiftTime shifttime, int branchId) {
        return getBranch(branchId).getAvailability(date, shifttime);
    }

    public List<List<Employee>> getAvailability(LocalDate from, LocalDate to, int branchId) {
        return getBranch(branchId).getAvailability(from, to);
    }

    public Shift getShift(LocalDate date, ShiftTime shifttime, int branchId) {
        return getBranch(branchId).getShift(date, shifttime);
    }

    public Shift[][] getSchedule(int branchID) {
        return getBranch(branchID).getScheduleAsMatrix();
    }


    //Employee Management Logic
    public boolean createNewEmployee(Employee newEmployee, int branchId) {
        return branches.get(branchId).createNewEmployee(newEmployee);
    }

    public boolean setEmployeeAsManger(long empID, int branchID) {
        return branches.get(branchID).setEmployeeAsManager(empID);
    }

    public boolean changeEmployeePosition(long empID, int branchID, Position position) {
        return branches.get(branchID).changeEmployeePosition(empID, position);
    }

    public boolean changeEmpBranch(long empID, int branchID, int newBranch) {
        if(!branches.keySet().contains(branchID) | !branches.keySet().contains(newBranch))return false;
        Employee employee = branches.get(branchID).deleteEmployee(empID);
        if (employee == null) return false;
        return branches.get(newBranch).createNewEmployee(employee);
    }

    public boolean deleteEmployee(long id, int branchId) {
        return branches.get(branchId).deleteEmployee(id) != null;
    }

    public boolean changeEmployeeId(long id, int branchid, long newID) {
        for (Branch branch : branches.values())
            for (Employee emp : branch.getStaff()) if (emp.getId() == newID) return false;
        return branches.get(branchid).changeEmployeeId(id, newID);
    }

    public void saveEmployee(Employee employee, int branchId) {
        branches.get(branchId).saveEmployee(employee);
    }

    public Map<Integer, LinkedList<Shift>> getOutDatedShifts() {
        Map<Integer, LinkedList<Shift>> output = new HashMap<>();
        for(Map.Entry<Integer, Branch> e : branches.entrySet()){
            int branchId = e.getKey();
            Branch branch = e.getValue();
            LinkedList<Shift> newHist = branch.getOutDatedShifts();
            output.put(branchId,newHist);
        }
        return output;
    }


    //End Region Getters and SETTERS

}
