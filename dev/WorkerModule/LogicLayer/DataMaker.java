package WorkerModule.LogicLayer;

import WorkerModule.presentationlayer.dataObjects.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class DataMaker {
    public static EmployeeData makeEmployeeData(Employee e, int branchId){
        return new EmployeeData(e.getName(),e.getId(),e.getBankAccountNo(),e.getSalary(),
                e.getConditions(),e.getPositions(),e.getFirstDay(),branchId);
    }
    public static BranchData makeBranchData(Branch branch){
        List<EmployeeData> emps = makeEmployeesData(branch.getStaff(),branch.getId());
        return new BranchData(branch.getId(),branch.getName(),emps);
    }
    public static FranchiseData makeFranchiseData(Franchise franchise){
        return null;
    }
    public static ShiftData makeShiftData(Shift shift, int branchId){
        List<EmployeeData> staff = makeEmployeesData(shift.getStaff(),branchId);
        return new ShiftData(staff,shift.getShiftTime(),shift.getDate(),branchId);
    }

    public static List<EmployeeData> makeEmployeesData(List<Employee> emps, int branchId) {
        LinkedList<EmployeeData> output = new LinkedList<EmployeeData>();
        for (Employee emp : emps)output.add(makeEmployeeData(emp, branchId));
        return output;

    }
    public static List<List<EmployeeData>> make2DEmployeesData(List<List<Employee>> emps2D, int branchId) {
        List<List<EmployeeData>> output = new LinkedList<List<EmployeeData>>();
        for(List<Employee> emps : emps2D){
            output.add(makeEmployeesData(emps,branchId));
        }
        return output;
    }
    public static List<BranchData> makeBranchesData(List<Branch> branches){
        List<BranchData> output = new ArrayList<BranchData>();
        for(Branch b :branches){
            output.add(makeBranchData(b));
        }
        output.sort(Comparator.comparingInt(BranchData::getId));
        return output;
    }

    public static List<ShiftData> makeShiftsData(List<Shift> shifts, int branchId){
        LinkedList<ShiftData> output = new LinkedList<ShiftData>();
        for(Shift s : shifts){
            output.add(makeShiftData(s,branchId));
        }
        //should sort
        return output;
    }
    public static ShiftData[][] makeScheduleData(Shift[][] schedule, int branchId) {
        ShiftData[][] output = new ShiftData[schedule.length][schedule[0].length];
        for(int x=0; x<schedule.length;x++){
            for(int y=0; y<schedule[0].length;y++){
                output[x][y]=makeShiftData(schedule[x][y],branchId);
            }
        }
        return output;
    }
    public static Employee makeEmployeeLogic(EmployeeData e){
        return new Employee(e.getName(),e.getId(), e.getBankAccountNumber(), e.getSalary(), e.getConditions(), e.getPositions(), e.getFirstDate());
    }



}
