package WorkerModule.LogicLayer;

import WorkerModule.Constants;
import WorkerModule.Enums.ShiftTime;
import WorkerModule.Enums.Position;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static WorkerModule.Constants.NUM_SHIFT_IN_DAY;
import static WorkerModule.Constants.SCHEDULE_WINDOW;

public class Branch {
    private int id;
    private String name;
    private List<Employee> staff;
    private Map<Employee, Map<LocalDate,boolean[]>> constraints;
    //private Map<LocalDate,List<Employee>[]> schedule;
    private List<Shift> schedule;


    public Branch(int id, String name, List<Employee> staff,Map<Employee,
            Map<LocalDate,boolean[]>> constraints,List<Shift> schedule) {
        this.id = id;
        this.name = name;
        this.staff = staff;
        this.constraints=constraints;
        this.schedule = schedule;
    }

    public Employee getEmployee(long empId){
        for(Employee emp : staff){
            if(emp.getId()==empId)return emp;
        }
        return null;
    }
    public List<Employee> getEmployees(long[] empIds){
        List<Employee> output = new LinkedList<Employee>();
        for(int i=0; i<empIds.length; i++){
            output.add(getEmployee(empIds[i]));
        }
        return output;
    }
    public Map<LocalDate,boolean[]> getConstraints(long empId){
        removeOldConstraints(getEmployee(empId));
        return constraints.get(getEmployee(empId));
    }
    public boolean updateConstraints(long empId, boolean[][] newConstraints) {
        //assumes you can freely update your shifts
        Map<LocalDate,boolean[]> empConstraints = new HashMap<LocalDate, boolean[]>();
        for(int i=0;i<newConstraints.length;i++){
            boolean[] times = newConstraints[i];
            empConstraints.put(LocalDate.now().plusDays(i),times);
        }
        Employee emp = getEmployee(empId);
        constraints.put(emp,empConstraints);
        return true;
    }
    private void removeOldConstraints(Employee emp){
        Map<LocalDate,boolean[]> empConstraints = constraints.get(emp);
        empConstraints.entrySet().removeIf(e -> e.getKey().isBefore(LocalDate.now()));
    }
    private void removeOldShiftsFromSchedule(){
        schedule.removeIf(shift ->shift.getDate().isBefore(LocalDate.now()));
    }

    //Begin Region Getters and SETTERS

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<Employee, Map<LocalDate, boolean[]>> getConstraints() {
        return constraints;
    }

    public void setConstraints(Map<Employee, Map<LocalDate, boolean[]>> constraints) {
        this.constraints = constraints;
    }


    public List<Employee> getStaff() {
        return staff;
    }

    public void setStaff(List<Employee> staff) {
        this.staff = staff;
    }
    //End Region Getters and SETTERS


    public void addEmployeesToShift(LocalDate date, ShiftTime shifttime, long[] empIds) {
        List<Employee> scheduledEmps= getShift(date,shifttime).getStaff();
        List<Employee> empsToAdd = getEmployees(empIds);
        for(Employee emp : empsToAdd){
            if(!scheduledEmps.contains(emp))scheduledEmps.add(emp);
        }
    }

    public void removeEmployeesFromShift(LocalDate date, ShiftTime shifttime, long[] empIds) {
        List<Employee> scheduledEmps= getShift(date,shifttime).getStaff();
        List<Employee> empsToRemove = getEmployees(empIds);
        scheduledEmps.removeAll(empsToRemove);
    }

    public List<Employee> getAvailability(LocalDate date, ShiftTime shifttime) {
        //emp to daily constraints
        List<Employee> availableEmps = new LinkedList<Employee>();
        for(Map.Entry<Employee, Map<LocalDate,boolean[]>> e : constraints.entrySet()){
            Employee emp = e.getKey();
            boolean canWork = e.getValue().get(date)[shifttime.ordinal()];
            if(canWork) availableEmps.add(emp);
        }
        return availableEmps;
    }

    public List<List<Employee>> getAvailability(LocalDate from, LocalDate to){
        LinkedList<List<Employee>> output = new LinkedList<List<Employee>>();
        for(int i=0; !from.plusDays(i).isAfter(to);i++){
            for(int time=0; time< Constants.NUM_SHIFT_IN_DAY;time++){
                output.addLast(getAvailability(from.plusDays(i),ShiftTime.values()[time]));
            }
        }
        return output;
    }

    public Shift getShift(LocalDate date, ShiftTime shifttime) {
        for (Shift shift : schedule) {
            if (shift.getDate().equals(date) & shift.getShiftTime().equals(shifttime)) return shift;
        }
        //if not found
        Shift shift = new Shift(date, shifttime, new LinkedList<Employee>());
        schedule.add(shift);
        return shift;
    }
    public Shift[][] getScheduleAsMatrix() {
        removeOldShiftsFromSchedule();
        Shift[][] output = new Shift[SCHEDULE_WINDOW][NUM_SHIFT_IN_DAY];
        Shift shift;
        Iterator<Shift> iter = schedule.iterator();
        int day=0;
        while (iter.hasNext()){
            shift=iter.next();
            int dayDiff = (int)ChronoUnit.DAYS.between(LocalDate.now(),shift.getDate());
            output[dayDiff][shift.getShiftTime().ordinal()] = shift;
        }
        for(int x=0; x<output.length;x++){
            for(int y=0; y<output[0].length;y++){
                if(output[x][y] == null)
                    output[x][y]=new Shift(LocalDate.now().plusDays(x),ShiftTime.values()[y],new LinkedList<Employee>());
            }
        }
        return output;
    }

    public void saveEmployee(Employee employee) {
        for(Employee emp : staff) if(emp.getId() == employee.getId()) emp.clone(employee);
    }

    public boolean changeEmployeeId(long id, long newID) {
        for (Employee emp :staff)
            if(emp.getId() == id){
                emp.setId(newID);
                return true;
            }
         return false;
    }

    public Employee deleteEmployee(long id) {
        Employee emp = getEmployee(id);
        staff.remove(emp);
        //staff, constraints, schedule
        for(Shift shift : schedule)
            shift.getStaff().remove(emp);
        constraints.remove(emp);
        return emp;
    }

    public boolean createNewEmployee(Employee employee) {
        staff.add(employee);
        return true;
    }

    public boolean changeEmployeePosition(long empID, Position position) {
        for(Employee emp :staff)
            if(emp.getId() == empID){
                List<Position> positions = emp.getPositions();
                for(int i = 0; i < positions.size(); i++){
                    if(positions.get(i) != Position.SHIFT_MANAGER){
                        positions.remove(i);
                        positions.add(position);
                        return true;
                    }
            }
        }
        return false;
    }

    public boolean setEmployeeAsManager(long empID) {
        boolean doNothing = false;
        for(Employee emp :staff)
            if(emp.getId() == empID){
                List<Position> positions = emp.getPositions();
                for(int i = 0; i < positions.size(); i++){
                    if(positions.get(i) == Position.SHIFT_MANAGER) doNothing = true;
                }
                if(!doNothing)positions.add(Position.SHIFT_MANAGER);
            }
        return false;
    }

    //for debugging purposes only!
    private void printConstraints(){
        System.out.println("\n\nLOGIC PRINT START");
        for(Map.Entry<Employee, Map<LocalDate,boolean[]>> e1 : constraints.entrySet()){
            Employee emp = e1.getKey();
            Map<LocalDate,boolean[]> empConstraints = e1.getValue();
            for(Map.Entry<LocalDate,boolean[]> e2 : empConstraints.entrySet()){
                LocalDate day = e2.getKey();
                boolean[]  edc = e2.getValue();
                System.out.println(day+"\t"+edc[0]+"\t"+edc[1]+"\t"+emp.getName());
            }
        }
        System.out.println("LOGIC PRINT END\n\n");
    }


    public LinkedList<Shift> getOutDatedShifts() {
        LinkedList<Shift> output = new LinkedList<>();
        for(Shift shift : schedule){
            if(shift.getDate().isBefore(LocalDate.now()))output.add(shift);
        }
        return output;
    }
}
