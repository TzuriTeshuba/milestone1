package WorkerModule.LogicLayer;

import WorkerModule.Enums.ShiftTime;

import java.time.LocalDate;
import java.util.List;

public class Shift implements Comparable {


    private int branchId;
    private LocalDate date;
    private ShiftTime shiftTime;
    private List<Employee> staff;

    public Shift(int branchId, LocalDate date, ShiftTime shiftTime, List<Employee> staff) {
        this.branchId = branchId;
        this.date = date;
        this.shiftTime = shiftTime;
        this.staff = staff;
    }

    public Shift(LocalDate date, ShiftTime shiftTime, List<Employee> staff) {
        this.date = date;
        this.shiftTime = shiftTime;
        this.staff = staff;
    }

    @Override
    public int compareTo(Object o) {
        if (!(o instanceof Shift)) return 0;//throw new Exception("expected Shift, received "+o.getClass().getName());
        Shift shift2 = (Shift) o;
        int dateComp = date.compareTo(shift2.date);
        if (dateComp != 0) return dateComp;
        return shiftTime.compareTo(shift2.shiftTime);
    }

    public boolean hasEmployee(long empId) {
        for (Employee emp : staff) {
            if (emp.getId() == empId) return true;
        }
        return false;
    }

    public boolean isSameTimeAs(Shift that) {
        if (that == null) return false;
        boolean output = true;
        if (!this.date.equals(that.date)) output = false;
        if (!this.shiftTime.equals(that.shiftTime)) output = false;
        return output;
    }

    public boolean equals(Shift that) {
        return (this.isSameTimeAs(that)) && (this.branchId == that.branchId);
    }

    public String toString() {
        String output = date.toString() + " " + shiftTime.toString() + " staff:\n";
        for (Employee emp : staff) {
            output += emp.getPositions() + " :\t" + emp.getName() + "\t" + emp.getId() + "\n";
        }
        return output;
    }

    //Begin Region Getters and SETTERS
    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public ShiftTime getShiftTime() {
        return shiftTime;
    }

    public void setShiftTime(ShiftTime shiftTime) {
        this.shiftTime = shiftTime;
    }

    public List<Employee> getStaff() {
        return staff;
    }

    public void setStaff(List<Employee> staff) {
        this.staff = staff;
    }


    //End Region Getters and SETTERS

}
