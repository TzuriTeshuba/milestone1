package WorkerModule.LogicLayer;

import WorkerModule.Constants;
import WorkerModule.Enums.Position;
import WorkerModule.Enums.ShiftTime;
import WorkerModule.LogicLayer.SupportedSystems.iHistoryViewer;
import WorkerModule.LogicLayer.SupportedSystems.iLogin;
import WorkerModule.LogicLayer.SupportedSystems.iSubmission;
import WorkerModule.presentationlayer.SystemApps.SystemApp;
import WorkerModule.presentationlayer.dataObjects.BranchData;
import WorkerModule.presentationlayer.dataObjects.EmployeeData;
import WorkerModule.presentationlayer.dataObjects.ShiftData;

import java.time.LocalDate;
import java.util.*;

import static WorkerModule.Enums.Position.SHIFT_MANAGER;

public class ServiceManager implements iLogin, iHistoryViewer, iSubmission {

    private static final int SUBMISSION_WINDOW = Constants.SUBMISSION_WINDOW;
    private static final int NUM_SHIFTS_IN_DAY = Constants.NUM_SHIFT_IN_DAY;

    private Franchise franchise;
    private Map<Integer, LinkedList<Shift>> history;
    private Map<Position, List<Class<? extends SystemApp>>> accesses;
    private static ServiceManager INSTANCE;

    public static ServiceManager getInstance() {
        if (INSTANCE == null) INSTANCE = new ServiceManager();
        return INSTANCE;
    }

    private ServiceManager() {
    }


    public void initData(int mode){
        DBDummy dbDummy = DBDummy.getInstance(mode);
        this.franchise = dbDummy.getFranchise();
        this.history = dbDummy.getHistory();
        this.accesses = dbDummy.getAccesses();
    }


    public ServiceManager(String franchise) {
        this.franchise = new Franchise();
    }

    //History Viewer Logic
    @Override
    public EmployeeData getEmployee(long empId) {
        for (EmployeeData emp : getAllDataEmployees())
            if (emp.getId() == empId) return emp;
        return null;
    }
    public List<ShiftData> getFranchiseHistory(LocalDate from, LocalDate to) {
        List<ShiftData> output = new LinkedList<ShiftData>();
        for(Map.Entry<Integer,LinkedList<Shift>> e : history.entrySet()){
            int branchId = e.getKey();
            output.addAll(getBranchHistory(branchId,from,to));
        }
        return output;
    }
    @Override
    public LinkedList<ShiftData> getBranchHistory(int branchId, LocalDate from, LocalDate to) {
        Shift first = new Shift(branchId, from, ShiftTime.MORNING, null);
        Shift last = new Shift(branchId, to, ShiftTime.EVENING, null);
        Shift currShift;
        LinkedList<ShiftData> output = new LinkedList<ShiftData>(); //FIFO
        LinkedList<Shift> branchHistory = history.get(branchId);
        if (branchHistory.isEmpty()) return output;
        Iterator<Shift> iter = branchHistory.iterator();
        //find first shift
        while (iter.hasNext()) {
            currShift = iter.next();
            if (currShift.compareTo(first) >= 0) {
                output.addLast(DataMaker.makeShiftData(currShift, branchId));
                break;
            }
        }
        while (iter.hasNext()) {
            currShift = iter.next();
            if(currShift.compareTo(last)>0)break;
            output.addLast(DataMaker.makeShiftData(currShift,branchId));
        }
        return output;
    }
    public List<ShiftData> getEmployeeHistory(int empId, LocalDate from, LocalDate to){
        EmployeeData emp = getEmployee(empId);
        if(emp==null)return null;
        return getEmployeeHistory(empId,emp.getBranchId(),from,to);
    }
    @Override
    public List<ShiftData> getEmployeeHistory(long empId, int branchId, LocalDate from, LocalDate to) {
        Shift first = new Shift(branchId, from, ShiftTime.MORNING, null);
        Shift last = new Shift(branchId, to, ShiftTime.EVENING, null);
        Shift currShift;
        LinkedList<ShiftData> output = new LinkedList<ShiftData>();
        LinkedList<Shift> branchHistory = history.get(branchId);
        if (branchHistory.isEmpty()) return output;
        Iterator<Shift> iter = branchHistory.iterator();
        //find first shift
        while (iter.hasNext()) {
            currShift = iter.next();
            if (currShift.compareTo(first) >= 0) {
                if (currShift.hasEmployee(empId)) output.addLast(DataMaker.makeShiftData(currShift, branchId));
                break;
            }
        }
        while (iter.hasNext()) {
            currShift = iter.next();
            if(currShift.compareTo(last)>0)break;
            if(currShift.hasEmployee(empId))output.addLast(DataMaker.makeShiftData(currShift,branchId));
        }
        return output;
    }

    @Override
    public Shift getShift(int branchId, LocalDate date, ShiftTime shiftTime) {
        Shift curr;
        Shift temp = new Shift(branchId, date, shiftTime, null);
        LinkedList<Shift> branchHistory = history.get(branchId);
        Iterator<Shift> iter = branchHistory.iterator();
        while (iter.hasNext()) {
            curr = iter.next();
            if (curr.isSameTimeAs(temp)) return curr;
        }
        return null;
    }

    @Override
    public List<EmployeeData> getStaff(int branchId, LocalDate date, ShiftTime shiftTime) {
        Shift shift = getShift(branchId, date, shiftTime);
        if (shift == null) return null;
        return DataMaker.makeEmployeesData(shift.getStaff(), branchId);
    }

    //Submission Logic
    @Override
    public boolean submitShifts(long empId, int branchId, boolean[][] newConstraints) {
        return franchise.submitShifts(empId, branchId, newConstraints);
    }

    @Override
    public boolean[][] getConstraints(long empId, int branchId) {
        boolean[][] output = new boolean[SUBMISSION_WINDOW][NUM_SHIFTS_IN_DAY];
        Map<LocalDate, boolean[]> constraints = franchise.getConstraints(empId, branchId);
        constraints.entrySet().removeIf(e -> e.getKey().isBefore(LocalDate.now()));
        for (int i = 0; i < SUBMISSION_WINDOW; i++) {
            boolean[] dayOut = new boolean[NUM_SHIFTS_IN_DAY];
            boolean[] dayIn = constraints.get(LocalDate.now().plusDays(i));
            for (int time = 0; time < NUM_SHIFTS_IN_DAY; time++) {
                dayOut[time] = dayIn[time];
            }
            output[i] = dayOut;
        }
        return output;
    }

    public Franchise getFranchise() {
        return franchise;
    }

    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;
    }

    public void saveEmpoyee(EmployeeData emp) {
        Employee employee = DataMaker.makeEmployeeLogic(emp);
        franchise.saveEmployee(employee, emp.getBranchId());
    }

    public boolean changeEmpoyeeId(long id, int branchid, long newID) {
        return franchise.changeEmployeeId(id, branchid, newID);
    }

    public List<EmployeeData> getAllDataEmployees() {
        List<EmployeeData> employees = new LinkedList<>();
        for (Branch branch : franchise.getBranches().values())
            employees.addAll(DataMaker.makeEmployeesData(branch.getStaff(), branch.getId()));
        return employees;
    }


    public List<BranchData> getBranches() {
        return DataMaker.makeBranchesData(new LinkedList<>(franchise.getBranches().values()));
    }

    public boolean deleteEmployee(long empID, int branchId) {
        return franchise.deleteEmployee(empID, branchId);
    }

    public boolean changeEmpBranch(long empID, int branchID, int newBranch) {
        return franchise.changeEmpBranch(empID, branchID, newBranch);
    }

    public ShiftData[][] getSchedule(int branchID) {
        Shift[][] schedule = franchise.getSchedule(branchID);
        return DataMaker.makeScheduleData(schedule, branchID);
    }

    public boolean changeEmployeePosition(long empID, int branchID, Position position) {
        return franchise.changeEmployeePosition(empID, branchID, position);
    }

    public boolean setEmployeeAsManager(long empID, int branchID) {
        return franchise.setEmployeeAsManger(empID, branchID);
    }

    public boolean createNewEmployee(EmployeeData e) {
        Employee newEmployee = DataMaker.makeEmployeeLogic(e);
        return franchise.createNewEmployee(newEmployee, e.getBranchId());
    }

    //Shift Assembler Logic
    public List<List<EmployeeData>> getAvailability(LocalDate from, LocalDate to, int branchId) {
        List<List<Employee>> empsByDate = franchise.getAvailability(from, to, branchId);
        return DataMaker.make2DEmployeesData(empsByDate, branchId);
    }

    public List<EmployeeData> getAvailabilty(ShiftData shift) {
        List<Employee> emps = franchise.getAvailability(shift.getDate(), shift.getShifttime(), shift.getBranchId());
        return DataMaker.makeEmployeesData(emps, shift.getBranchId());
    }

    public void removeEmployeesFromShift(ShiftData shift, long[] empIds) {
        franchise.removeEmployeesFromShift(shift.getDate(), shift.getShifttime(), shift.getBranchId(), empIds);
        Shift updatedShift = franchise.getShift(shift.getDate(), shift.getShifttime(), shift.getBranchId());
    }

    public void addEmployeesToShift(ShiftData shift, long[] empIds) {
        franchise.addEmployeesToShift(shift.getDate(), shift.getShifttime(), shift.getBranchId(), empIds);
        Shift updatedShift = franchise.getShift(shift.getDate(), shift.getShifttime(), shift.getBranchId());
    }

    public ShiftData getShiftFromSchedule(ShiftData shift) {
        return DataMaker.makeShiftData(franchise.getShift(shift.getDate(), shift.getShifttime(), shift.getBranchId()), shift.getBranchId());
    }

    public List<EmployeeData> getAvailableManagers(ShiftData shift) {
        List<EmployeeData> emps = getAvailabilty(shift);
        emps.removeIf(emp -> !emp.getPositions().contains(SHIFT_MANAGER));
        return emps;
    }




    public boolean checkEmployeeIdAvailability(int idToCheck) {
        for (EmployeeData emp : getAllDataEmployees()) if (emp.getId() == idToCheck) return false;
        return true;
    }


    public void updateHistory() {
        //remove old history

        for(Map.Entry<Integer, LinkedList<Shift>> e : history.entrySet()){
            LinkedList<Shift> shifts = e.getValue();
            shifts.removeIf(s->s.getDate().plusMonths(Constants.MONTHS_HISTORY_BACKUP).isBefore(LocalDate.now()));
        }

        //add new history
        Map<Integer, LinkedList<Shift>> toAdd = franchise.getOutDatedShifts();
        for(Map.Entry<Integer, LinkedList<Shift>> e : toAdd.entrySet()){
            int branchId = e.getKey();
            LinkedList<Shift> newHist = e.getValue();
            LinkedList<Shift> oldHist = history.get(branchId);
            oldHist.addAll(newHist);
        }
    }
}
