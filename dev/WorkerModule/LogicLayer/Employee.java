package WorkerModule.LogicLayer;

import WorkerModule.Enums.Position;

import java.time.LocalDate;
import java.util.List;

public class Employee {
    private String name;
    private long id;
    private int bankAccountNo;
    private int salary;
    private String conditions;
    private List<Position> positions;
    private LocalDate firstDay;

    public Employee(String name, long id, int bankAccountNo, int salary, String conditions,
                    List<Position> positions, LocalDate firstDay) {
        this.name = name;
        this.id = id;
        this.bankAccountNo = bankAccountNo;
        this.salary = salary;
        this.conditions = conditions;
        this.positions = positions;
        this.firstDay = firstDay;
    }

    public void clone(Employee e) {
        this.name = e.name;
        this.id = e.id;
        this.bankAccountNo = e.bankAccountNo;
        this.salary = e.salary;
        this.positions = e.positions;
        this.conditions = e.conditions;
    }


    //Begin Region GETTERS and SETTERS

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getBankAccountNo() {
        return bankAccountNo;
    }

    public void setBankAccountNo(int bankAccountNo) {
        this.bankAccountNo = bankAccountNo;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    public List<Position> getPositions() {
        return positions;
    }

    public void setPositions(List<Position> positions) {
        this.positions = positions;
    }

    public LocalDate getFirstDay() {
        return firstDay;
    }

    public void setFirstDay(LocalDate firstDay) {
        this.firstDay = firstDay;
    }

    //End Region GETTERS and SETTERS

}
