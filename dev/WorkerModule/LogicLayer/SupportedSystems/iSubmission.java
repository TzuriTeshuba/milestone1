package WorkerModule.LogicLayer.SupportedSystems;

public interface iSubmission {

    public boolean submitShifts(long empId, int branchId, boolean[][] constraints);

    public boolean[][] getConstraints(long empId, int branchId);


}
