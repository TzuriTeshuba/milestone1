package WorkerModule.LogicLayer.SupportedSystems;

import WorkerModule.presentationlayer.dataObjects.EmployeeData;

public interface iEmployeeManager {
    public boolean addEmployee(EmployeeData empData);
}
