package WorkerModule.LogicLayer.SupportedSystems;


import WorkerModule.presentationlayer.dataObjects.EmployeeData;

public interface iLogin {
    public EmployeeData getEmployee(long empId);

}
