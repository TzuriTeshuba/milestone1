package WorkerModule.LogicLayer.SupportedSystems;

import WorkerModule.Enums.ShiftTime;
import WorkerModule.LogicLayer.Shift;
import WorkerModule.presentationlayer.dataObjects.EmployeeData;
import WorkerModule.presentationlayer.dataObjects.ShiftData;

import java.time.LocalDate;
import java.util.List;

public interface iHistoryViewer {
    public List<ShiftData> getBranchHistory(int branchId, LocalDate from, LocalDate to);

    public List<ShiftData> getEmployeeHistory(long empId, int branchId, LocalDate from, LocalDate to);

    public Shift getShift(int branchId, LocalDate date, ShiftTime shiftTime);

    public List<EmployeeData> getStaff(int branchId, LocalDate date, ShiftTime shiftTime);

}
