package WorkerModule.LogicLayer;

import WorkerModule.Constants;
import WorkerModule.Enums.Position;
import WorkerModule.Enums.ShiftTime;
import WorkerModule.presentationlayer.SystemApps.SystemApp;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static WorkerModule.Constants.NUM_SHIFT_IN_DAY;
import static WorkerModule.Constants.SUBMISSION_WINDOW;
import static WorkerModule.Enums.Position.*;

public class DBDummy {
    //hard coded initialized values
    private static final int NUM_BRANCHES = 3;
    private static final int NUM_POSITIONS_OPTIONS = 4;
    private static final int NUM_CONSTRAINTS_OPTS = 4;
    //private Map<Integer,Map<Employee, Map<LocalDate,boolean[]>>> constraints;
    private static DBDummy INSTANCE;
    private static int currEmpId = 100;
    private static List<List<Employee>> empsByBranch = initEmps();
    private static List<List<Shift>> schedulesByBranch = schedulesByBranch();
    //constraints
    private static boolean[] yy(){ return new boolean[]{true, true};}
    private static boolean[] yn(){ return new boolean[]{true, false};}
    private static boolean[] ny(){ return new boolean[]{false, true};}
    private static boolean[] nn(){ return new boolean[]{false, false};}

    private Franchise franchise;
    private Map<Integer, LinkedList<Shift>> history;
    private Map<Position, List<Class<? extends SystemApp>>> accesses;

    private DBDummy(int mode) {
        this.accesses = Constants.accesses;
        if(mode==Constants.AUTO_INIT) {
            this.franchise = new Franchise("Super Lee", makeBranches());
            this.history = makeHistory();
        }
        else{
            this.franchise = makeEmptyFranchise();
            this.history= new HashMap<Integer, LinkedList<Shift>>();
            history.put(9999, new LinkedList<Shift>());
        }
    }

    public static DBDummy getInstance(int mode) {
        if (INSTANCE == null) INSTANCE = new DBDummy(mode);
        return INSTANCE;
    }


    //Begin Region Getters and SETTERS

    private static Map<Integer, LinkedList<Shift>> makeHistory() {
        Map<Integer, LinkedList<Shift>> output = new HashMap<Integer, LinkedList<Shift>>();
        LocalDate first = LocalDate.of(2020, 02, 01);
        LocalDate date;
        for (int b = 0; b < NUM_BRANCHES; b++) {
            LinkedList<Shift> branchHistory = new LinkedList<Shift>();
            for (int day = 0; day < 10; day++) {
                date = first.plusDays(day);
                List<Employee> generalStaff = empsByBranch.get(b);
                List<Employee> empsThatWorked = new LinkedList<>();
                for (int e = 0; e < 4; e++) {
                    empsThatWorked.add(generalStaff.get(e));
                }
                Shift shift = new Shift(date, ShiftTime.MORNING, empsThatWorked);
                branchHistory.add(shift);
            }
            output.put(b, branchHistory);
        }
        return output;
    }

    private static Map<Integer, Branch> makeBranches() {
        Map<Integer, Branch> output = new HashMap<Integer, Branch>();
        for (int i = 0; i < NUM_BRANCHES; i++) {
            Map<Employee, Map<LocalDate, boolean[]>> branchConstraints = new HashMap<Employee, Map<LocalDate, boolean[]>>();
            List<Employee> emps = empsByBranch.get(i);
            for (Employee emp : emps) {
                Map<LocalDate, boolean[]> empConstraints = constraintsOpts().get(i % NUM_CONSTRAINTS_OPTS);
                branchConstraints.put(emp, empConstraints);
            }
            Branch branch = new Branch(i, "bran" + i, emps, branchConstraints, schedulesByBranch().get(i));
            output.put(branch.getId(), branch);
        }
        return output;
    }

    private static List<List<Position>> positionsOptions() {
        List<List<Position>> output = new LinkedList<List<Position>>();
        List<Position> pos1 = new LinkedList<>();
        List<Position> pos2 = new LinkedList<>();
        List<Position> pos3 = new LinkedList<>();
        List<Position> pos4 = new LinkedList<>();
        pos1.add(HR_MANAGER);
        pos2.add(CASHIER);
        pos3.add(SHIFT_MANAGER);
        pos3.add(CASHIER);
        pos4.add(DRIVER);
        pos4.add(GUARD);
        output.add(pos1);
        output.add(pos2);
        output.add(pos3);
        output.add(pos4);
        return output;
    }

    private static List<List<Employee>> initEmps() {
        List<List<Employee>> output = new LinkedList<List<Employee>>();
        LocalDate firstDay = LocalDate.parse("2020-01-01");
        for (int i = 0; i < NUM_BRANCHES; i++) {
            currEmpId = 100 + (100 * i);
            List<Employee> emps = new LinkedList<Employee>();
            for (char c = 'a'; c < 'o'; c++) {
                Employee emp = new Employee("emp" + i + c, currEmpId, 1234, 200,
                        "conditions", positionsOptions().get(c % NUM_POSITIONS_OPTIONS), firstDay);
                emps.add(emp);
                //System.out.println(""+currEmpId+"\t"+emp.getPositions());
                currEmpId++;
            }
            output.add(emps);
        }
        return output;
    }

    private static List<Map<LocalDate, boolean[]>> constraintsOpts() {
        List<Map<LocalDate, boolean[]>> output = new LinkedList<Map<LocalDate, boolean[]>>();
        Map<LocalDate, boolean[]> allMorning = new HashMap<LocalDate, boolean[]>();
        Map<LocalDate, boolean[]> allEvening = new HashMap<LocalDate, boolean[]>();
        Map<LocalDate, boolean[]> allOdd = new HashMap<LocalDate, boolean[]>();
        Map<LocalDate, boolean[]> allEven = new HashMap<LocalDate, boolean[]>();
        LocalDate today = LocalDate.now();
        LocalDate date = today;
        for (int day = 0; day < SUBMISSION_WINDOW; day++) {
            date = today.plusDays(day);
            allMorning.put(date, yn());
            allEvening.put(date, ny());
            if (day % 2 == 0) {
                allEven.put(date, yy());
                allOdd.put(date, nn());
            } else {
                allEven.put(date, nn());
                allOdd.put(date, yy());
            }

        }
        output.add(allMorning);
        output.add(allEvening);
        output.add(allEven);
        output.add(allOdd);
        return output;
    }

    private static List<List<Shift>> schedulesByBranch() {
        List<List<Shift>> schedules = new LinkedList<List<Shift>>();
        for (int b = 0; b < NUM_BRANCHES; b++) {
            List<Shift> schedule = new LinkedList<Shift>();
            for (int day = 0; day < Constants.SCHEDULE_WINDOW; day++) {
                LocalDate date = LocalDate.now().plusDays(day);
                for (int time = 0; time < NUM_SHIFT_IN_DAY; time++) {
                    Shift shift = new Shift(b, date, ShiftTime.values()[time], new LinkedList<Employee>());
                    schedule.add(shift);
                }
            }
            schedules.add(schedule);
        }
        return schedules;
    }
    //End Region Getters and SETTERS


    public Franchise getFranchise() {
        return franchise;
    }

    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;
    }

    public Map<Integer, LinkedList<Shift>> getHistory() {
        return history;
    }

    public void setHistory(Map<Integer, LinkedList<Shift>> history) {
        this.history = history;
    }

    public Map<Position, List<Class<? extends SystemApp>>> getAccesses() {
        return accesses;
    }

    public void setAccesses(Map<Position, List<Class<? extends SystemApp>>> accesses) {
        this.accesses = accesses;
    }



    /*
    Initialize Empty
     */

    private static List<Shift> makeDefaultSchedule(){
        List<Shift> output = new LinkedList<>();
        for(int i=0; i<Constants.SCHEDULE_WINDOW;i++){
            Shift shift1 = new Shift(9999,LocalDate.now().plusDays(i),ShiftTime.MORNING,new LinkedList<Employee>());
            Shift shift2 = new Shift(9999,LocalDate.now().plusDays(i),ShiftTime.EVENING,new LinkedList<Employee>());
        }
        return output;
    }
    private static Map<Employee,Map<LocalDate,boolean[]>> makeDefaultEmpConstraints(Employee emp){
        Map<Employee,Map<LocalDate,boolean[]>> m1 = new HashMap<>();
        Map<LocalDate,boolean[]> map1a = new HashMap<>();
        for(int i=0; i< Constants.SUBMISSION_WINDOW;i++){
            map1a.put(LocalDate.now().plusDays(i), new boolean[]{false,false});
        }
        m1.put(emp,map1a);
        return m1;
    }

    private static Franchise makeEmptyFranchise(){
        Franchise output = new Franchise();
        output.setName("Super Lee");
        Map<Integer,Branch>branches = new HashMap<>();
        List<Position> positions = new LinkedList<>();
        positions.add(Position.HR_MANAGER);
        Employee emp = new Employee("hrman",9999,9999,0,"",
                positions,LocalDate.now());
        List<Employee> emps = new LinkedList<>();
        emps.add(emp);
        Map<Employee,Map<LocalDate,boolean[]>> empCons = makeDefaultEmpConstraints(emp);
        List<Shift> shifts = makeDefaultSchedule();
        Branch branch = new Branch(9999,"Default", emps,empCons,shifts);
        branches.put(9999,branch);
        output.setBranches(branches);
        return output;
    }
}
