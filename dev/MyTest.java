import WorkerModule.Constants;
import WorkerModule.Enums.Position;
import WorkerModule.Enums.ShiftTime;
import WorkerModule.LogicLayer.ServiceManager;
import WorkerModule.presentationlayer.dataObjects.EmployeeData;
import WorkerModule.presentationlayer.dataObjects.ShiftData;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.LinkedList;

public class MyTest {
    ServiceManager service = ServiceManager.getInstance();

    @Before
    public void init(){
        service.initData(Constants.AUTO_INIT);
    }
    @After
    public void finilize(){
    }


    @Test
    public void testDeleteEmployee() {
        service.deleteEmployee(103,0);
        Assert.assertNull((service.getEmployee(103)));

    }

    @Test
    public void testAddEmployee(){
        service.createNewEmployee(new EmployeeData("bob", 123,123
                ,123,"123", Arrays.asList(Position.HR_MANAGER), LocalDate.now(),1));
        Assert.assertNotNull(service.getEmployee(123));
    }

    @Test
    public void testChangeEmployeeID_1(){
        service.changeEmpoyeeId(300,2,999);
        Assert.assertNotNull(service.getEmployee(999).getBranchId());
    }

    @Test
    public void testChangeEmployeeID_2(){
        Assert.assertFalse(service.changeEmpoyeeId(103,0,104));
    }

    @Test
    public void testChangeEmployeeBranch_1(){
        service.changeEmpBranch(100,0,1);
        Assert.assertEquals(1, service.getEmployee(100).getBranchId());
    }
    @Test
    public void testChangeEmployeeBranch_2(){
        Assert.assertFalse(service.changeEmpBranch(101,0, 5));
    }

    @Test
    public void testGetShift(){
        long empIDs[] = {201,202,203};
        ShiftData shiftData = new ShiftData(new LinkedList<EmployeeData>(), ShiftTime.MORNING, LocalDate.now(), 1);
        service.addEmployeesToShift(shiftData, empIDs);
        ShiftData shift = service.getShiftFromSchedule(shiftData);
        Assert.assertFalse(shift.getStaff().isEmpty());
    }

    @Test
    public void testCheckAvailableID_1(){
        Assert.assertTrue(service.checkEmployeeIdAvailability(908823));
    }

    @Test
    public void testCheckAvailableID_2(){
        Assert.assertFalse(service.checkEmployeeIdAvailability(203));
    }

    @Test
    public void checkSetEmployeeAsManager(){
        service.setEmployeeAsManager(302,2);
        Assert.assertTrue(service.getEmployee(302).getPositions().contains(Position.SHIFT_MANAGER));
    }
}
